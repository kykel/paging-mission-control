#Language: Python3.4
#Author: Mykel Byrnes
#Title: Paging Mission Control
#Date: 7/19/2020

import datetime
import json

class PagingMissionControl():

	def __init__(self):
		self.battTracker = {}
		self.tempTracker = {}
		self.alerts = []

	#Checks if conditions match for necessary alert
	def monitor(self, line):
		data = line.split("|")
		#Is my battery at critically low levels?
		if line.endswith("BATT"):
			if float(data[-2]) < float(data[-3]):
				self.battChecker(data[1],data)
		#Is my temperature to high?
		elif line.endswith("TSTAT"):
			if float(data[-2]) > float(data[2]):
				self.tstatChecker(data[1],data)

	#Checks against previous battery alerts and removes alerts that exceed time limit or alerts on those within
	def battChecker(self,satID,data):
		if satID in self.battTracker:
			tempLog = self.battTracker[satID].copy()
		else:
			self.battTracker[satID] = []

		if len(self.battTracker[satID]) >= 2:
			for log in tempLog:
				timeframe = self.compareTimes(self.formatDateTime(log),self.formatDateTime(data[0]))
				if timeframe > 300:
					self.battTracker[satID].remove(log)
		
		self.battTracker[satID].append(data[0])
		
		if len(self.battTracker[satID]) >= 3:
			self.alert("RED LOW","BATT",data)

	#Checks against previous temperature alerts and removes alerts that exceed time limit or alerts on those within		
	def tstatChecker(self,satID,data):
		if satID in self.tempTracker:
			tempLog = self.tempTracker[satID].copy()
		else:
			self.tempTracker[satID] = []

		if len(self.tempTracker[satID]) >= 2:
			for log in tempLog:
				timeframe = self.compareTimes(self.formatDateTime(log),self.formatDateTime(data[0]))
				if timeframe > 300:
					self.tempTracker[satID].remove(log)
		
		self.tempTracker[satID].append(data[0])

		if len(self.tempTracker[satID]) >= 3:
			self.alert("RED HIGH","TSTAT",data)

	#Calculates the difference between 2 dates/times and returns in seconds
	def compareTimes(self,oldtimestamp,newtimestamp):
		timeformat = '%Y-%m-%d %H:%M:%S.%f'
		timePassed = datetime.datetime.strptime(newtimestamp,timeformat) - datetime.datetime.strptime(oldtimestamp,timeformat)
		return timePassed.seconds

	#Converts dates to standard format
	def formatDateTime(self,time):
		return time[:4]+"-"+time[4:6]+"-"+time[6:8]+time[8:]

	#Generates alerts and triggers output
	def alert(self, severity, component, data):
		alert = {}
		alert["satelliteId"] = int(data[1])
		alert["severity"] = severity
		alert["component"] = component
		alert["timestamp"] = data[0][:4]+"-"+data[0][4:6]+"-"+data[0][6:8]+"T"+data[0][9:10]+data[0][10:]+"Z"
		self.alerts.append(alert)
		self.output()

	def output(self):
		print(json.dumps(self.alerts, indent=4, sort_keys=True),"\n")
		

if __name__ == "__main__":
	filename = "testdata.txt"
	#Emulates incoming input from satellite
	pmc = PagingMissionControl()
	txtFile = open(filename)
	rawData = txtFile.readlines()
	txtFile.close()

	for line in rawData:
		pmc.monitor(line.strip())
	